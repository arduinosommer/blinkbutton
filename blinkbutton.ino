//En variabel för att se om knappen ar nedtryckt eller inte
int switchState = LOW;

//Konfiguration
void setup() {
  /* Våran LED lampa är kopplad till pin tre, 
   * som vi vill ge ström till när vi ska tända
   * LED lampan
   */
  pinMode(3, OUTPUT);
  
  /* På pin 2 vill vi kolla om vi har ström på eller inte
   * om vi har ström så är knappen nertryckt
   */
  pinMode(2, INPUT); 
}

void loop() {
  /* Läser av port 2 för att kolla om vi har ström här eller inte
   * (DVS att vi kollar om knappen är nedtryckt eller inte)
   */
  switchState = digitalRead(2);
  
  if(switchState == LOW) { //Om knappen inte är nedtryckt
    digitalWrite(3, LOW); //Släck lampan 
  } 
  else { //Om knappen är nedtryckt
    digitalWrite(3, HIGH); //Tänd lampan
  }
}
